#encoding: utf-8
require 'digest/sha1'
require 'yaml'
require_relative 'ResourceMaker'
#Digest::SHA1.hexdigest str
#Digest::SHA1.file(filename).hexdigest
module Replicator
  #< Data def
  Project = Struct.new :name, :public_name, :version, :dependencies, :includes, :shares, :instigator, :priority, :blocked_resources
  Archive = Struct.new :project, :files, :file_offsets, :frags, :frag_offsets
  Signature = Struct.new :size, :mtime, :sha1
  module PW
    module_function
    def log_in(credential)
      # Remplacer par le vrai code de connexion
      if(credential == "0123456789abcdfe")
        return 1
      else
        return 0 #< Fail
      end
    end
    
    def find_pwr_by_name(name)
      return nil
    end
    
    def find_project_by_name(name)
      name.downcase!
      case name
      when "pokemonfe"
        return Project.new(name, "Pokémon Forêt Éternelle", 0, [], [], [], 59, 100, [])
      when "psdk"
        return Project.new(name, "Pokémon SDK", 0, [], [], [], 1, 0, [])
      when "pokemonprisme"
        return Project.new(name, "Pokémon Prisme", 0, [], [], [], 68, 100, [])
      when "pokemonbremo"
        return Project.new(name, "Pokémon Brêmo", 0, [], [], [], 58, 100, [])
      when "pokemonrenaissance"
        return Project.new(name, "Pokémon Renaissance", 0, [], [], [], 4019, 100, [])
      when "pokemondimension"
        return Project.new(name, "Pokémon Dimension", 0, [], [], [], 406, 100, [])
      end
      return nil
    end
  end
  
  class User
    attr_reader :id
    def initialize
      @id = 0
    end
    
    def log_in(credential)
      @id = PW.log_in(credential)
      return @id != 0
    end
    
  end
  
  #> Config section
  @configs = {
    credential: "0123456789abcdfe",
    projects: [],
    project_names: [],
    download_pwr_shares: true,
    project_min_priority: 0,
    project_max_priority: 100_000,
    number_of_versions: 10,
    try_to_host: true,
    force_port: 0
  }
  
  module_function
  def adjust_type(object, klass, *args)
    return (object.class == klass ? object : klass.new(*args))
  end
  
  CONF_FILE = "conf/globals.yaml"
  PROJECT_CONF_FILE = "%{path}conf/%{name}.yaml"
  PROJECT_INCL_FILE = "%{path}conf/includes.yaml"
  PROJECT_SHAR_FILE = "%{path}conf/shares.yaml"
  PROJECT_BLOC_FILE = "%{path}conf/blocked.yaml"
  def load_configs
    return false unless File.exist?(CONF_FILE)
    File.open(CONF_FILE) do |f|
      configs = YAML.load(f)
      @configs[:credential] = configs[:credential].to_s
      @configs[:projects] = adjust_type(configs[:projects], Array)
      @configs[:project_names] = adjust_type(configs[:project_names], Array)
      @configs[:download_pwr_shares] = configs[:download_pwr_shares] == true
      @configs[:project_min_priority] = configs[:project_min_priority].to_i & 0x1FFFF
      @configs[:project_max_priority] = configs[:project_max_priority].to_i & 0x1FFFF
      @configs[:number_of_versions] = configs[:number_of_versions].to_i.abs
      @configs[:try_to_host] = configs[:try_to_host] == true
      @configs[:force_port] = configs[:force_port].abs
    end
    return true
  rescue Exception
    puts "Failed to load config file in %{path} : %{error} %{message}".%(path: File.expand_path('.'), error: $!.class, message: $!.message)
    return false
  end
  
  def save_configs
    File.open(CONF_FILE, "wb") do |f|
      YAML.dump(@configs, f)
    end
  rescue Exception
    puts "Failed to save config file in %{path} : %{error} %{message}".%(path: File.expand_path('.'), error: $!.class, message: $!.message)
  end
  
  
  def start
    puts "Failed to load config... Using default." unless load_configs
    @user = User.new
    puts "Failed to log into Pokémon Workshop" unless @user.log_in(@configs[:credential])
    @current_share_building = nil
  end
  
  def get_user
    @user
  end
end
Replicator.start