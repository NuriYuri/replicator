#encoding: utf-8
module Replicator
  Resource = Struct.new :name, :public_name, :path, :type, :tags, :instigator, :credits, :url
  Resource::Types = [:Autotile, :Character, :Picture, :Tileset, :Chipset, :Sprite, :Icon, :Interface, :BGM, :SE, :Audio, :Script, :Plugin, :Pack]
  module ResourceMaker
    @current_ressource = nil
    module_function
    
    def start(name, public_name, relative_path_in_project, type)
      return false unless check_up_start(name, type)
      @cd = File.expand_path('.')
      @current_ressource = Resource.new(name, public_name, relative_path_in_project.downcase, type, [], Replicator.get_user.id, {}, @base_url = "replicator://shares/#{name}")
      @file_hash = nil
      @frag_hash = nil
      start_cmd
    end
    
    def start_cmd
      while(print(@cd, " > ");cmd = gets.chomp)
        cmd, *args = cmd.split(' ')
        case cmd
        when 'finish'
          finish
          break
        when 'add_tag'
          add_tag(*args)
        when 'credits'
          add_credits(args.join(' '), nil)
        when 'clear_credits'
          @current_ressource.credits.delete(args.join(' '))
        when 'del_tag'
          del_tag(*args)
        when 'cd'
          cd(args.join(' '))
        when 'ls'
          ls(args.join(' '))
        when 'set_url'
          set_url(args.join(' '))
        when 'add_file'
          add_file(args.join(' '))
        end
      end
    end
    
    def set_url(url)
      return unless check_resource
      if (url =~ (/http(s|):\/\/.*/)) != 0
        puts "Invalid url, reset to #@base_url"
        @current_ressource.url = @base_url
      else
        @current_ressource.url = url
      end
    end
    
    def add_file(filename)
      return unless check_resource
      filename = "#@cd/#{filename}"
      unless File.exist?(filename)
        puts "#{filename} not found..."
      end
      init_pack
      sub_filename = filename.gsub("#@cd/", '')
      sub_filename.downcase!
      unless @file_hash[sub_filename]
        File.open(filename) do |f|
          @files << f.read(f.size)
        end
        @file_hash[sub_filename] = @file_offset
        @file_offset += @files.last.bytesize
      end
    rescue Exception
      puts "Failed to load file %{filename} : %{error} %{message}".%(filename: filename, error: $!.class, message: $!.message)
    end
    
    def init_pack
      unless @file_hash
        @file_hash = {}
        @files = []
        @file_offset = 0
        @frag_hash = {}
        @frags = []
        @frag_offset = 0
      end
    end
    
    def ls(selector)
      if(selector.empty?)
        dir = Dir["#@cd/*"]
      else
        dir = Dir["#@cd/#{selector}"]
      end
      dir.each do |path| path.sub!(@cd, '');path.sub!('/', '') end
      puts dir.join("\t")
    end
    
    def cd(path)
      if path == '..'
        path = @cd.split('/')
        path.pop
        @cd = path.join('/')
      elsif Dir.exist?(@cd + '/' + path)
        @cd = @cd + '/' + path
      elsif Dir.exist?(path)
        @cd = path
      else
        puts "Invalid path."
      end
    end
    
    def add_tag(*args)
      return unless check_resource
      args.each do |arg| arg.gsub!(/[^a-z0-9_]/, '') end
      @current_ressource.tags.concat(args.select { |arg| !arg.empty? })
      @current_ressource.tags.uniq!
      puts "tags : %{tags}".%(tags: @current_ressource.tags.join(', '))
    end
    
    def del_tag(*args)
      return unless check_resource
      args.each do |arg| arg.gsub!(/[^a-z0-9_]/, '') end
      @current_ressource.tags.delete_if { |tag| args.include?(tag) }
      puts "tags : %{tags}".%(tags: @current_ressource.tags.join(', '))
    end
    
    def add_credits(category, list)
      return unless check_resource
      credits = @current_ressource.credits[category] || []
      if list
        credits.concat(list)
      else
        puts "Enter list and end by an empty line :"
        while(name = gets.chomp)
          break if name.empty?
          credits.concat(name.split(','))
        end
      end
      credits.each do |credit| credit.strip! end
      credits.uniq!
      @current_ressource.credits[category] = credits
    end
    
    def check_resource
      @current_ressource != nil
    end
    
    def finish
      return unless check_resource
      filename = "shares/"+@current_ressource.name
      File.open(filename, "w") do |f|
        Marshal.dump(@current_ressource, f)
        if(@file_hash)
          arch = Archive.new(nil, @file_hash.keys, @file_hash.values.push(@file_offset), @frag_hash.keys, @frag_hash.values)
          Marshal.dump(arch, f)
          @files.each do |data|
            f << data
          end
          @frags.each do |data|
            f << data
          end
        end
      end
      puts "%{public_name} saved !".%(public_name: @current_ressource.public_name)
      @current_ressource = nil
    rescue Exception
      puts "Failed to save file %{filename} : %{error} %{message}".%(filename: filename, error: $!.class, message: $!.message)
    end
    
    def check_up_start(name, type)
      if name =~ /[^a-z0-9_]/
        puts "Name contain invalid chars... Accepted : a-z 0-9"
        return false
      end
      if @current_ressource
        puts "Already making a resource : %{public_name}.".%(public_name: @current_ressource.public_name)
        return false
      end
      if res = PW.find_pwr_by_name(name)
        puts "%{name} is a resource on PW (%{public_name}).".%(name: res.name, public_name: res.public_name)
        return false
      end
      unless Resource::Types.include?(type)
        puts "Invalid typ %{type}, accepted types : %{types}".%(type: type, types: Resource::Types.join(', '))
        return false
      end
      return true
    end
  end
end